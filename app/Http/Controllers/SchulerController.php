<?php

namespace App\Http\Controllers;

use App\Models\KlassenLehrer;
use App\Models\Schuler;
use Illuminate\Http\Request;

class SchulerController extends Controller
{
    public function getSchueler() {
        return response()->json(Schuler::all(), 200);
    }

    public function getSchulerById($id) {
        $schuler = Schuler::find($id);
        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($schuler::find($id), 200);
    }

    public function addSchuler(Request $request) {
        $lehrer = KlassenLehrer::where('name', '=' , $request->klassenLehrer)->first();
        
        if(is_null($lehrer)) {
            $lehrer = KlassenLehrer::create([
                'name' => $request->klassenLehrer,
            ]);
        }
        
        $schuler = Schuler::create([
        'name' => $request->name,
        'geburtsdatum'=> $request->geburtsdatum,
        'email'=> $request->email,
        'stadt'=> $request->stadt,
        'strasse'=> $request->strasse,
        'klasse' => $request->klasse,
        'postleitzahl'=> $request->postleitzahl,
        'klassenLehrer' => $lehrer->id
        ]);

        return response($schuler, 201);
    }

    public function updateSchuler(Request $request, $id) {
        $schuler = Schuler::find($id);

        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }

        $schuler->update($request->all());
        return response($schuler, 200);
    }

    public function deleteSchuler(Request $request, $id) {
        $schuler = Schuler::find($id);

        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }

        $schuler->delete();
        return response()->json(null, 204);
    }

    public function findSchulerByName(Request $request) {
        $schuler = Schuler::where('name', 'like','%'.$request->name.'%')->get();

        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($schuler);
    }

    public function findSchulerByClass(Request $request) {
        $schuler = Schuler::where('klasse', 'like','%'.$request->klasse.'%')->get();

        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($schuler);
    }

    public function findSchulerByNameOrClass(Request $request) {
        $schuler = Schuler::where('klasse', 'like','%'.$request->klasse.'%')
                          ->where('name', 'like','%'.$request->name.'%')
                          ->get();

        if(is_null($schuler)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($schuler);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function loginUser(Request $request) {
        $user = User::where('email', $request->email)->first();
        
        if ($user) {
            if ($user->password == $request->password) {
                return response()->json([
                    'message' => 'Access granted',
                    'status'  => 1,
                ]);
            } else {
                return response()->json([
                    'message' => 'Access denied',
                    'status'  => 0,
                ]);
            }
        }

        return response()->json([
            'message' => 'Email not match',
            'code'    => '404',
            'status'  => 0,
        ]);
    }

    public function registerUser(Request $request) {
        $user = User::create($request->all());
        return response($user, 201);
    }

    public function getUsers() {
        return response()->json(User::all());
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\KlassenLehrer;
use Illuminate\Http\Request;

class KlassenLehrerController extends Controller
{
    public function addKlassenLehrer(Request $request) {
        $lehrer = KlassenLehrer::create($request->all());
        return response($lehrer, 201);
    }

    public function getKlassenLehrer() {
        return response()->json(KlassenLehrer::all(), 200);
    }

    public function getKlassenLehrerById($id) {
        $klassenLehrer = KlassenLehrer::find($id);
        if(is_null($klassenLehrer)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($klassenLehrer::find($id), 200);
    }

    public function deleteKlassenLehrer(Request $request, $id) {
        $klassenLehrer = KlassenLehrer::find($id); 
        
        if(is_null($klassenLehrer)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }

        $klassenLehrer->delete();
        return response()->json(null, 204);
    }

    public function findKlassenLehrerByName(Request $request) {
        $klassenLehrer = KlassenLehrer::where('name', 'like','%'.$request->name.'%')->get();

        if(is_null($klassenLehrer)) {
            return response()->json(['message' => 'Schuler Not found'], 404);
        }
        return response()->json($klassenLehrer);
    }
}

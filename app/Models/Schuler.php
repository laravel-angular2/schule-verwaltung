<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schuler extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'name',
        'email',
        'stadt',
        'strasse',
        'postleitzahl',
        'klasse',
        'geburtsdatum',
        'klassenLehrer'
    ];
}
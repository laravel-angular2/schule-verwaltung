<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Get all schueler
Route::get('schueler', 'App\Http\Controllers\SchulerController@getSchueler');

// Get a schuler by id 
Route::get('schuler/{id}', 'App\Http\Controllers\SchulerController@getSchulerById');

// Add a new schuler
Route::post('addSchuler', 'App\Http\Controllers\SchulerController@addSchuler');

// Update schuler data
Route::put('updateSchuler/{id}', 'App\Http\Controllers\SchulerController@updateSchuler');

// Delelte Schuler
Route::delete('deleteSchuler/{id}', 'App\Http\Controllers\SchulerController@deleteSchuler');

// Get a schuler by name and class
Route::get('/search/klasse={klasse}&name={name}', 'App\Http\Controllers\SchulerController@findSchulerByNameOrClass');

// Get a schuler by name
Route::get('/search/name={name}', 'App\Http\Controllers\SchulerController@findSchulerByName');

// Get a schuler by class
Route::get('/search/klasse={klasse}', 'App\Http\Controllers\SchulerController@findSchulerByClass');

// login
Route::post('login', 'App\Http\Controllers\UserController@loginUser');

// Get All users
Route::get('users', 'App\Http\Controllers\UserController@getUsers');

// Add a user
Route::post('register', 'App\Http\Controllers\UserController@registerUser');



// Get all Klassenlehrer
Route::get('klassenLehrer', 'App\Http\Controllers\KlassenLehrerController@getKlassenLehrer');

// Get a klassen lehrer by id 
Route::get('klassenLehrer/{id}', 'App\Http\Controllers\KlassenLehrerController@getKlassenLehrerById');

// Add a new Lehrer
Route::post('addKlassenLehrer', 'App\Http\Controllers\KlassenLehrerController@addKlassenLehrer');

// Delelte Lehrer
Route::delete('deleteKlassenLehrer/{id}', 'App\Http\Controllers\KlassenLehrerController@deleteKlassenLehrer');

// find lehrer by name
Route::get('/searchLehrer/name={name}', 'App\Http\Controllers\KlassenLehrerController@findKlassenLehrerByName');
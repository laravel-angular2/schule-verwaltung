import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SchulerTableComponent } from './components/schuler-table/schuler-table.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NewSchulerComponent } from './components/new-schuler/new-schuler.component';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { EditSchulerModalComponent } from './components/edit-schuler-modal/edit-schuler-modal.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatInputModule} from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';
import { SearchSchulerComponent } from './components/search-schuler/search-schuler.component';
import { LoginComponent } from './components/login/login.component';
import { RouterModule } from '@angular/router';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material/select';
import { LehrerTableComponent } from './components/lehrer-table/lehrer-table.component';

@NgModule({
  declarations: [
    AppComponent,
    SchulerTableComponent,
    NavbarComponent,
    NewSchulerComponent,
    EditSchulerModalComponent,
    SearchSchulerComponent,
    LoginComponent,
    LehrerTableComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatSlideToggleModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatDividerModule,
    RouterModule,
    MatFormFieldModule,
    MatSelectModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

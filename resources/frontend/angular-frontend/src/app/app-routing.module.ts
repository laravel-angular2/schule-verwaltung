import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SchulerTableComponent } from './components/schuler-table/schuler-table.component';
import { NewSchulerComponent } from './components/new-schuler/new-schuler.component';
import { SearchSchulerComponent } from './components/search-schuler/search-schuler.component';
import { LoginComponent } from './components/login/login.component';
import { AuthGuardService } from './services/auth-guard.service';
import { LehrerTableComponent } from './components/lehrer-table/lehrer-table.component';

const routes: Routes = [
  {
    path: 'schuelerTable',
    component: SchulerTableComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'lehrerTable',
    component: LehrerTableComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'addSchuler',
    component: NewSchulerComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'searchSchuler',
    component: SearchSchulerComponent,
    canActivate: [AuthGuardService]
  },
  { 
    path: '',   
    redirectTo: '/schuelerTable', 
    pathMatch: 'full' 
  },
  {
    path: 'login',
    component: LoginComponent,
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  user: User = new User;
  data: any;

  loginForm = this.fb.group({
    email: ['', Validators.required],
    password: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private dataService: DataService, private authGuardService: AuthGuardService, private router: Router) {}

  login() {
    this.user.email = this.loginForm.get('email')?.value;
    this.user.password = this.loginForm.get('password')?.value;
    this.dataService.login(this.user).subscribe(res => {
      this.data = res;
      if (this.data.status === 0) {
        console.log(this.data.message);
      } else {
        console.log(this.data.message);
        localStorage.setItem('verwaltung-schuler-accessed', 'true');
        this.router.navigate(['/schuelerTable']);
      }
    });
  }
}

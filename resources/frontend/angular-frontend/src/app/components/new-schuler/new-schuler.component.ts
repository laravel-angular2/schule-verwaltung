import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { KlassenLehrer } from 'src/app/models/klassenLehrer';
import { Schuler } from 'src/app/models/schuler';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-new-schuler',
  templateUrl: './new-schuler.component.html',
  styleUrls: ['./new-schuler.component.scss']
})
export class NewSchulerComponent {

  schuler: Schuler = new Schuler;
  lehrer: KlassenLehrer = new KlassenLehrer;

  selected = "schueler";

  schulerForm = this.fb.group({
    name: ['', Validators.required],
    geburtsdatum: ['', Validators.required],
    email: ['', Validators.required],
    stadt: ['', Validators.required],
    strasse: ['', Validators.required],
    postleitzahl: ['', Validators.required],
    klasse: ['', Validators.required],
    klassenLehrer: ['', Validators.required]
  });

  lehrerForm = this.fb.group({
    name: ['', Validators.required],
    geburtsdatum: ['', Validators.required],
    email: ['', Validators.required],
  });

  constructor(private fb: FormBuilder, private dataService: DataService) {}

  insertSchueler() {
    this.schuler.name = this.schulerForm.get('name')?.value;
    this.schuler.geburtsdatum = this.schulerForm.get('geburtsdatum')?.value;
    this.schuler.email = this.schulerForm.get('email')?.value;
    this.schuler.stadt = this.schulerForm.get('stadt')?.value;
    this.schuler.strasse = this.schulerForm.get('strasse')?.value;
    this.schuler.postleitzahl = this.schulerForm.get('postleitzahl')?.value;
    this.schuler.klasse = this.schulerForm.get('klasse')?.value;
    this.schuler.klassenLehrer = this.schulerForm.get('klassenLehrer')?.value;

    this.dataService.addSchuler(this.schuler);

    this.schulerForm.reset();
  }

  insertLehrer() {
    this.lehrer.name = this.lehrerForm.get('name')?.value;
    this.lehrer.geburtsdatum = this.lehrerForm.get('geburtsdatum')?.value;
    this.lehrer.email = this.lehrerForm.get('email')?.value;

    this.dataService.addKlassenLehrer(this.lehrer);

    this.lehrerForm.reset();
  }
}

import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-lehrer-table',
  templateUrl: './lehrer-table.component.html',
  styleUrls: ['./lehrer-table.component.scss']
})
export class LehrerTableComponent implements OnInit {

  constructor(private dataService: DataService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataService.getLehrer();
  }

  get lehrer() {
    return this.dataService.lehrer;
  }

  removeLehrer(id:any) {
    this.dataService.removeLehrer(id).subscribe(() => {
      this.dataService.getLehrer();
    });
  }
}
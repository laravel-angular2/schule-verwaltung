import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { DataService } from 'src/app/services/data.service';
import { FormBuilder, Validators } from '@angular/forms';
import { Schuler } from 'src/app/models/schuler';

@Component({
  selector: 'app-edit-schuler-modal',
  templateUrl: './edit-schuler-modal.component.html',
  styleUrls: ['./edit-schuler-modal.component.scss']
})
export class EditSchulerModalComponent {

  constructor(
    public dialogRef: MatDialogRef<EditSchulerModalComponent>,
    private fb: FormBuilder, 
    private dataService: DataService
    ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  schuler: Schuler = new Schuler;

  schulerForm = this.fb.group({
    name: [this.dataService.editSchuler.name, Validators.required],
    geburtsdatum: [this.dataService.editSchuler.geburtsdatum, Validators.required],
    email: [this.dataService.editSchuler.email, Validators.required],
    stadt: [this.dataService.editSchuler.stadt, Validators.required],
    strasse: [this.dataService.editSchuler.strasse, Validators.required],
    postleitzahl: [this.dataService.editSchuler.postleitzahl, Validators.required],
    klasse: [this.dataService.editSchuler.klasse, Validators.required],
    klassenLehrer: [this.dataService.editSchuler.klassenLehrer, Validators.required],
  });

  insertData() {
    this.schuler.name = this.schulerForm.get('name')?.value;
    this.schuler.geburtsdatum = this.schulerForm.get('geburtsdatum')?.value;
    this.schuler.email = this.schulerForm.get('email')?.value;
    this.schuler.stadt = this.schulerForm.get('stadt')?.value;
    this.schuler.strasse = this.schulerForm.get('strasse')?.value;
    this.schuler.postleitzahl = this.schulerForm.get('postleitzahl')?.value;
    this.schuler.klasse = this.schulerForm.get('klasse')?.value;
    this.schuler.klassenLehrer = this.schulerForm.get('klassenLehrer')?.value;

    this.dataService.updateSchuler(this.schuler, this.dataService.editSchuler.id).subscribe(() => {
      this.dataService.getSchueler();
    });
    this.onNoClick();
  }
}

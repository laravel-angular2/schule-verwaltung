import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { MatDialog } from '@angular/material/dialog';
import { EditSchulerModalComponent } from '../edit-schuler-modal/edit-schuler-modal.component';
import { Schuler } from 'src/app/models/schuler';

@Component({
  selector: 'app-schuler-table',
  templateUrl: './schuler-table.component.html',
  styleUrls: ['./schuler-table.component.scss']
})
export class SchulerTableComponent implements OnInit {

  constructor(private dataService: DataService, public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.dataService.getSchueler();
  }

  get schueler() {
    return this.dataService.schueler;
  }

  removeSchuler(id:any) {
    this.dataService.removeSchuler(id).subscribe(() => {
      this.dataService.getSchueler();
    });
  }

  editSchuler(schuler:Schuler) {
    this.dataService.editSchuler = schuler;
    this.dialog.open(EditSchulerModalComponent, {
            width: '60%'
    });
  }
}

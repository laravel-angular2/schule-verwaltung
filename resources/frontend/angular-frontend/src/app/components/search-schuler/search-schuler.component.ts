import { Component } from '@angular/core';
import { Schuler } from 'src/app/models/schuler';
import { DataService } from 'src/app/services/data.service';
import { EditSchulerModalComponent } from '../edit-schuler-modal/edit-schuler-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-search-schuler',
  templateUrl: './search-schuler.component.html',
  styleUrls: ['./search-schuler.component.scss']
})
export class SearchSchulerComponent {

  schueler: any;
  lehrer: any;
  selected = "schueler";

  searchSchuelerForm = this.fb.group({
    name: [''],
    klasse: [''],
  });

  searchLehrerForm = this.fb.group({
    name: [''],
  });

  constructor(private dataService: DataService, public dialog: MatDialog, private fb: FormBuilder) {}

  removeSchuler(id:any) {
    this.dataService.removeSchuler(id).subscribe(() => {
      this.searchSchueler();
    });
  }

  editSchuler(schuler: Schuler) {
    this.dataService.editSchuler = schuler;
    this.dialog.open(EditSchulerModalComponent, {
            width: '60%'
    });
  }

  searchSchueler() {
    let name = this.searchSchuelerForm.get('name')?.value;
    let klasse = this.searchSchuelerForm.get('klasse')?.value;
    
    if (name && klasse) {
      this.dataService.searchSchulerByNameAndClass(klasse, name).subscribe(res => {
        this.schueler = res;
      });
    } else if (name) {
        this.dataService.searchSchulerByName(name).subscribe(res => {
          this.schueler = res;
        });
    } else if (klasse) {
        this.dataService.searchSchulerByClass(klasse).subscribe(res => {
          this.schueler = res;
        });
    }
  }

  searchLehrer() {
    let name = this.searchLehrerForm.get('name')?.value;
    
    this.dataService.searchLehrer(name).subscribe(res => {
      this.lehrer = res;
    });
  }

  removeLehrer(id:any) {
    this.dataService.removeLehrer(id).subscribe(() => {
      this.searchLehrer();
    });
  }
}

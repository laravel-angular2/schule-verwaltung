import { Injectable, isDevMode } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Schuler } from '../models/schuler';
import { environment } from 'src/environments/environment';
import { KlassenLehrer } from '../models/klassenLehrer';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  editSchuler: Schuler = new Schuler;
  schueler: any;
  lehrer: any;

  apiUrl = environment.apiUrl;

  constructor(private httpClient: HttpClient) {
  }

  getSchueler() {
    return this.httpClient.get(this.apiUrl + 'schueler').subscribe(res => {
      this.schueler = res;
    });
  }

  addSchuler(schuler: Schuler) {
    return this.httpClient.post(this.apiUrl + 'addSchuler', schuler).subscribe();
  }

  removeSchuler(id: any) {
    return this.httpClient.delete(this.apiUrl + 'deleteSchuler/' + id);
  }

  updateSchuler(schuler: Schuler, id: any) {
    return this.httpClient.put(this.apiUrl + 'updateSchuler/' + id, schuler);
  }

  searchSchulerByNameAndClass(klasse: any, name: any) {
    return this.httpClient.get(this.apiUrl + 'search/klasse=' + klasse + '&name=' + name);
  }

  searchSchulerByName(name: any) {
    return this.httpClient.get(this.apiUrl + 'search/name=' + name);
  }

  searchSchulerByClass(klasse: any) {
    return this.httpClient.get(this.apiUrl + 'search/klasse=' + klasse);
  }

  login(data: any) {
    return this.httpClient.post(this.apiUrl + 'login' , data);;
  }

  getLehrer() {
    return this.httpClient.get(this.apiUrl + 'klassenLehrer').subscribe(res => {
      this.lehrer = res;
    });
  }

  addKlassenLehrer(klassenLehrer: KlassenLehrer) {
    return this.httpClient.post(this.apiUrl + 'addKlassenLehrer', klassenLehrer).subscribe();
  }

  removeLehrer(id: any) {
    return this.httpClient.delete(this.apiUrl + 'deleteKlassenLehrer/' + id);
  }

  searchLehrer(name: any) {
    return this.httpClient.get(this.apiUrl + 'searchLehrer/name=' + name);
  }
}

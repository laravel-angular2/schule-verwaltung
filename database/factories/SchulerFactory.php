<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Schuler>
 */
class SchulerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->name(),
            'geburtsdatum' => fake()->date(),
            'email' => fake()->unique()->safeEmail(),
            'stadt' => fake()->city(),
            'strasse' => fake()->streetName(),
            'postleitzahl' => fake()->randomNumber(5, true),
            'klasse' => fake()->regexify('[A-C]{1}[1-3]{1}'),
        ];
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('schulers', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->date('geburtsdatum');
            $table->string('email');
            $table->string('stadt');
            $table->string('strasse');
            $table->integer('postleitzahl');
            $table->string('klasse');
            $table->integer('klassenLehrer')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('schulers');
    }
};

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('klassen_lehrers', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->date('geburtsdatum')->nullable();
            $table->string('email')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('klassen_lehrers');
    }
};
